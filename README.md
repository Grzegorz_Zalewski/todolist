# ToDoList

This project was crated to learn React

CI static test bulid:
[![Build Status](https://semaphoreci.com/api/v1/zalewskig/todolist/branches/master/badge.svg)](https://semaphoreci.com/zalewskig/todolist)

Acceptance Tests bulid:
[![Acceptance Tests](https://semaphoreci.com/api/v1/zalewskig/todolist-2/branches/master/badge.svg)](https://semaphoreci.com/zalewskig/todolist-2)

[demo](http://react-mobx-todolist.herokuapp.com)

## Used libraries

* [semantic-ui-react](http://react.semantic-ui.com) official React integration for: [Semantic UI](http://semantic-ui.com/)
* [mobx-react](https://github.com/mobxjs/mobx-react) I decided to use MobX instead of Redux due to the size of this app additionally, observer pattern are closer to my heart. :) 
* [velocity-react](https://github.com/twitter-fabric/velocity-react) React components for interacting with the Velocity DOM animation library.
* [cucumber.js](https://github.com/cucumber/cucumber-js) Tool for running automated tests.

### credits

* Favicon made by [Madebyoliver](href="http://www.flaticon.com/authors/madebyoliver) from www.flaticon.com
* This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
I had to eject build tool to add decorators support.