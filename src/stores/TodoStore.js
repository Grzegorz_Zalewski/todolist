import { observable, computed, reaction } from 'mobx';
import TodoModel from '../models/TodoModel';
import * as Utils from '../common/utils';
import { ALL_TODOS, ACTIVE_TODOS, COMPLETED_TODOS } from '../common/constants';


export default class TodoStore {
  @observable todos = [];
  @observable searchFilter = '';
  @observable todoBeingEdited = null;
  @observable todoFilter= ALL_TODOS;

  @computed get activeTodoCount() {
    return this.filteredTodos.reduce(
        (sum, todo) => sum + (todo.completed ? 0 : 1),
        0
    );
  }

  @computed get completedCount() {
    return this.todos.length - this.activeTodoCount;
  }

  @computed get filteredTodos() {
    const matchesFilter = new RegExp(this.searchFilter, 'i');
    return this.todos.filter(todo => !this.searchFilter || matchesFilter.test(todo.title))
        .filter((todo) => {
          switch (this.todoFilter) {
          case ACTIVE_TODOS:
            return !todo.completed;
          case COMPLETED_TODOS:
            return todo.completed;
          default:
            return true;
          }
        });
  }

  subscribeLocalStorageToStore() {
    reaction(
        () => this.toJS(),
        todos => localStorage.setItem('todos', JSON.stringify({ todos }))
    );
  }


  addTodo(title) {
    this.todos.push(new TodoModel(this, Utils.uuid(), title, false));
  }

  toggleAll(checked) {
    this.todos.forEach(
      todo => todo.done(checked)
    );
  }

  clearCompleted() {
    this.todos = this.todos.filter(
        todo => !todo.completed
    );
  }

  toJS() {
    return this.todos.map(todo => todo.toJS());
  }

  static fromJS(array) {
    const todoStore = new TodoStore();
    todoStore.todos = array.map(item => TodoModel.fromJS(todoStore, item));
    return todoStore;
  }

  static reconstructFormLocalStorage() {
    const initialState = JSON.parse(localStorage.getItem('todos')) || {};
    return this.fromJS(initialState.todos || []);
  }
}
