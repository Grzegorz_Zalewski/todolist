import React from 'react';
import { observer } from 'mobx-react';
import { Label } from 'semantic-ui-react';
import { pluralize } from '../common/utils';


  @observer
export default class TodoCounter extends React.Component {
  render() {
    const todoStore = this.props.todoStore;
    if (!todoStore.activeTodoCount) { return null; }
    const activeTodoWord = pluralize(todoStore.activeTodoCount, 'item');
    return (
      <Label color="red" ribbon className="task counter"><strong>{todoStore.activeTodoCount}</strong> {activeTodoWord} left</Label>
    );
  }
}

TodoCounter.propTypes = {
  todoStore: React.PropTypes.object.isRequired
};
