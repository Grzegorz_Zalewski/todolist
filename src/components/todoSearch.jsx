import React from 'react';
import { Input } from 'semantic-ui-react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';


@observer
export default class TodoSearch extends React.Component {
  @observable searchText = '';

  handleChange = (event) => {
    this.searchText = event.target.value;
    this.props.todoStore.searchFilter = this.searchText;
  };

  render() {
    return (
      <Input
        fluid className="search"
        icon="search" placeholder="Search..."
        value={this.searchText}
        onChange={this.handleChange}
      />
    );
  }
}

TodoSearch.propTypes = {
  todoStore: React.PropTypes.object.isRequired
};
