import React from 'react';
import { observer } from 'mobx-react';
import { Router } from 'director';
import { Container, Header, Card, Icon } from 'semantic-ui-react';
import { VelocityTransitionGroup } from 'velocity-react';
import TodoItem from './todoItem';
import TodoCounter from './todoCounter';
import TodoSearch from './todoSearch';
import TodoAdd from './todoAdd';
import TodoFooter from './todoFooter';


import { ALL_TODOS, ACTIVE_TODOS, COMPLETED_TODOS } from '../common/constants';

@observer
export default class TodoList extends React.Component {
  componentDidMount() {
    const todoStore = this.props.todoStore;
    const router = Router({
      '/': function showAll() { todoStore.todoFilter = ALL_TODOS; },
      '/active': function showActive() { todoStore.todoFilter = ACTIVE_TODOS; },
      '/completed': function showCompleted() { todoStore.todoFilter = COMPLETED_TODOS; }
    });
    router.init('/');
  }
  getVisibleTodos() {
    return this.props.todoStore.filteredTodos;
  }
  toggleAll = (event) => {
    const checked = event.target.checked;
    this.props.todoStore.toggleAll(checked);
  };


  render() {
    const { todoStore } = this.props;
    return (
      <div className="App">
        <Header as="h1" icon textAlign="center">
          <Icon name="unordered list" />
          <Header.Content>
              To do List
            </Header.Content>
        </Header>
        <Container>
          <Card fluid>
            <Card.Content className="ui top attached segment">
              <TodoSearch todoStore={todoStore} />
              <TodoCounter todoStore={todoStore} />
              <VelocityTransitionGroup component="div" className="ui divided items" enter={{ animation: 'fadeIn' }} leave={{ animation: 'fadeOut' }}>
                {this.getVisibleTodos().map(todo =>
                      (<TodoItem
                        key={todo.id}
                        todo={todo}
                        todoStore={todoStore}
                      />)
                  )}
              </VelocityTransitionGroup>

              <TodoAdd todoStore={todoStore} />
            </Card.Content >
          </Card>
          <TodoFooter todoStore={todoStore} />
        </Container>
      </div>
    );
  }
}

TodoList.propTypes = {
  todoStore: React.PropTypes.object.isRequired
};
