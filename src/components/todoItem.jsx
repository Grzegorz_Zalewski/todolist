import React from 'react';
import { Item, Icon, Input } from 'semantic-ui-react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { ENTER_KEY, ESCAPE_KEY } from '../../src/common/constants';

@observer
export default class TodoItem extends React.Component {
  @observable editText = '';
  @observable focus = false;

  handleSubmit = () => {
    const val = this.editText.trim();
    if (val) {
      this.props.todo.setTitle(val);
      this.editText = val;
    } else {
      this.handleDestroy();
    }
    this.props.todoStore.todoBeingEdited = null;
  };

  handleDestroy = () => {
    this.props.todo.destroy();
    this.props.todoStore.todoBeingEdited = null;
  };

  handleEdit = () => {
    const todo = this.props.todo;
    this.props.todoStore.todoBeingEdited = todo;
    this.editText = todo.title;
  };

  handleKeyDown = (event) => {
    if (event.which === ESCAPE_KEY) {
      this.editText = this.props.todo.title;
      this.props.todoStore.todoBeingEdited = null;
    } else if (event.which === ENTER_KEY) {
      this.handleSubmit(event);
    }
  };

  handleChange = (event) => {
    this.editText = event.target.value;
  };

  handleToggle = () => {
    this.props.todo.toggle();
  };

  render() {
    const { todoStore, todo } = this.props;
    let icon = null;
    let cssClass = null;
    if (todo.completed) {
      icon = <Icon onClick={this.handleToggle} name="checkmark box" color="blue" size="large" />;
      cssClass = 'done';
    } else {
      if (todo === todoStore.todoBeingEdited) {
        cssClass = 'editing';
      }
      icon = <Icon onClick={this.handleToggle} name="circle thin" size="large" />;
    }

    return (
      <Item className={cssClass} onDoubleClick={this.handleEdit}>
        <Item.Content verticalAlign="middle">
          <Item.Header>
            {icon}
            { todo.title }
          </Item.Header>
          <Item.Extra>
            <Icon name="remove" className="remove todo" color="red" onClick={this.handleDestroy} />
          </Item.Extra>
          <Input
            className="todo fluid"
            value={this.editText}
            onBlur={this.handleSubmit}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDown}
          />
        </Item.Content>

      </Item>

    );
  }
}

TodoItem.propTypes = {
  todo: React.PropTypes.object.isRequired,
  todoStore: React.PropTypes.object.isRequired
};
