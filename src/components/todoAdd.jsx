import React from 'react';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import { Input, Form, Button } from 'semantic-ui-react';

@observer
export default class TodoAdd extends React.Component {
  @observable editText = '';

  handleChange = (event) => {
    this.editText = event.target.value;
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const value = this.editText.trim();

    if (value) {
      this.props.todoStore.addTodo(value);
      this.editText = '';
    }
  };

  render() {
    return (
      <Form className="add task" onSubmit={this.handleSubmit}>
        <Form.Field className="ui action fluid input">
          <Input
            type="text"
            className="right labeled"
            placeholder="Task..."
            name="newTodo"
            value={this.editText}
            onChange={this.handleChange}
          />
          <Button color="blue" content="Add" size="large" />
        </Form.Field>
      </Form>
    );
  }
}

TodoAdd.propTypes = {
  todoStore: React.PropTypes.object.isRequired
};

