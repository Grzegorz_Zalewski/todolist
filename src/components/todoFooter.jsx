import React from 'react';
import { observer } from 'mobx-react';
import { Icon, Menu } from 'semantic-ui-react';
import { ALL_TODOS, ACTIVE_TODOS, COMPLETED_TODOS } from '../common/constants';


@observer
export default class TodoFooter extends React.Component {

  clearCompleted = () => {
    this.props.todoStore.clearCompleted();
  };

  renderFilterLink(filterName, url, caption) {
    return (
      <Menu.Item name="active" active={filterName === this.props.todoStore.todoFilter} href={`#/${url}`}>
        {caption}
      </Menu.Item>
    );
  }

  render() {
    const todoStore = this.props.todoStore;
    if (!todoStore.activeTodoCount && !todoStore.completedCount) { return null; }

    return (
      <footer className="footer">
        <Menu attached="bottom" tabular>
          {this.renderFilterLink(ALL_TODOS, '', 'All')}
          {this.renderFilterLink(ACTIVE_TODOS, 'active', 'Active')}
          {this.renderFilterLink(COMPLETED_TODOS, 'completed', 'Completed')}
          { todoStore.completedCount === 0
            ? null
            : <Menu.Menu position="right" >
              <Menu.Item name="new-tab" className="ui computer only grid" active={false} onClick={this.clearCompleted}>
                <Icon name="remove" />
                Clear completed
              </Menu.Item>
            </Menu.Menu>

          }
        </Menu>
      </footer>
    );
  }
}

TodoFooter.propTypes = {

  todoStore: React.PropTypes.object.isRequired
};
