export function uuid() {
/* eslint no-bitwise: 0*/
/* eslint no-nested-ternary: 0*/
/* eslint no-mixed-operators: 0*/
/* eslint no-plusplus: 0*/
  let i;
  let random;
  let uuidValue = '';

  for (i = 0; i < 32; i++) {
    random = Math.random() * 16 | 0;
    if (i === 8 || i === 12 || i === 16 || i === 20) {
      uuidValue += '-';
    }
    uuidValue += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
        .toString(16);
  }

  return uuidValue;
}

export function pluralize(count, word) {
  return count === 1 ? word : `${word}s`;
}
