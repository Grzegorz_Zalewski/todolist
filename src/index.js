import React from 'react';
import ReactDOM from 'react-dom';
import TodoList from './components/todoList';
import '../semantic/dist/semantic.min.css';
import './index.css';
import TodoStore from './stores/TodoStore';

const todoStore = TodoStore.reconstructFormLocalStorage();

todoStore.subscribeLocalStorageToStore();

ReactDOM.render(React.createElement(TodoList, { todoStore }),
  document.getElementById('todoapp')
);
