module.exports = {
    "parser": "babel-eslint",
    "env": {
        "browser": true,
        "es6": true,
        "jquery": true,
      "jest": true,
      "node": true,
    },

    "parserOptions": {
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "jest"
    ],
    "extends": "airbnb",
    "rules": {
        "comma-dangle": [
            "warn",
            "never"
        ],
        "react/forbid-prop-types":0,
        "indent": [
            "warn",
            2
        ],
        "linebreak-style": [
            "warn",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        /* Advanced Rules*/
        "no-unused-expressions": "warn",
        "no-useless-concat": "warn",
        "block-scoped-var": "error",
        "consistent-return": "error"
    }
};