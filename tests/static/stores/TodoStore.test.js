import TodoStore from '../../../src/stores/TodoStore';
import { ACTIVE_TODOS, COMPLETED_TODOS } from '../../../src/common/constants';


describe('TodoStore the static output test', () => {
  // don't use an arrow function...preserve the value of "this"
  beforeEach(function () {
    this.todostore = new TodoStore();
    this.todostore.addTodo('todo1');
    this.todostore.addTodo('todo2');
    this.todostore.addTodo('lorem ipsum');
  });

  it('creates new todos', function () {
    expect(this.todostore.todos.length).toBe(3);
    expect(this.todostore.todos[0].title).toBe('todo1');
    expect(this.todostore.todos[1].title).toBe('todo2');
  });
  it('clears checked todos', function () {
    this.todostore.todos[1].completed = true;
    this.todostore.todos[2].completed = true;
    expect(this.todostore.completedCount).toBe(2);
    this.todostore.clearCompleted();
    expect(this.todostore.todos.length).toBe(1);
    expect(this.todostore.todos[0].title).toBe('todo1');
  });

  it('toggle all todos', function () {
    expect(this.todostore.completedCount).toBe(0);
    this.todostore.toggleAll(true);
    expect(this.todostore.completedCount).toBe(3);
    this.todostore.toggleAll(false);
    expect(this.todostore.completedCount).toBe(0);
  });
  it('filter todos by text', function () {
    this.todostore.searchFilter = 'ipsum';
    expect(this.todostore.filteredTodos.length).toBe(1);
    expect(this.todostore.filteredTodos[0].title).toBe('lorem ipsum');
  });
  it('filter todos by state', function () {
    this.todostore.todos[1].completed = true;
    this.todostore.todos[2].completed = true;
    this.todostore.todoFilter = ACTIVE_TODOS;
    expect(this.todostore.filteredTodos.length).toBe(1);
    this.todostore.todoFilter = COMPLETED_TODOS;
    expect(this.todostore.filteredTodos.length).toBe(2);
  });
  it('check active todos count', function () {
    expect(this.todostore.activeTodoCount).toBe(3);
    this.todostore.searchFilter = 'ipsum';
    expect(this.todostore.activeTodoCount).toBe(1);
    this.todostore.todos[1].completed = true;
    this.todostore.searchFilter = '';
    expect(this.todostore.activeTodoCount).toBe(2);
  });
});
