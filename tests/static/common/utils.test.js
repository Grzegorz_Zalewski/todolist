import * as Utils from '../../../src/common/utils';

describe('Utils the static output test', () => {
  it('creates new uuid', () => {
    const firstUuid = Utils.uuid();
    const secondUuid = Utils.uuid();
    const thirdUuid = Utils.uuid();
    const fourthUuid = Utils.uuid();
    const fifthUuid = Utils.uuid();
    expect(firstUuid).not.toBe(secondUuid);
    expect(firstUuid).not.toBe(thirdUuid);
    expect(firstUuid).not.toBe(fourthUuid);
    expect(firstUuid).not.toBe(fifthUuid);
  });
  it('pluralize text', () => {
    const oneItem = Utils.pluralize(1, 'item');
    const twoItems = Utils.pluralize(2, 'item');
    expect(oneItem).toBe('item');
    expect(twoItems).toBe('items');
  });
});
