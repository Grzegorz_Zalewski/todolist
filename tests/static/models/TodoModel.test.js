import TodoModel from '../../../src/models/TodoModel';

describe('TodoModel - the static output test', () => {
   // don't use an arrow function...preserve the value of "this"
  beforeEach(function () {
    this.todo = {
      id: 1,
      title: 'test todo',
      completed: false
    };
    this.todoModel = TodoModel.fromJS({}, this.todo);
  });

  it('correctly read the model', function () {
    expect(this.todoModel.toJS()).toEqual(this.todo);
  });
  it('toggled todo', function () {
    this.todoModel.toggle();
    expect(this.todoModel.toJS().completed).toBe(true);
    this.todoModel.toggle();
    expect(this.todoModel.toJS().completed).toBe(false);
  });
  it('set status to done', function () {
    this.todoModel.done(true);
    expect(this.todoModel.toJS().completed).toBe(true);
  });
  it('set new todo title', function () {
    this.todoModel.setTitle('Title changed');
    expect(this.todoModel.toJS().title).toBe('Title changed');
  });
});
