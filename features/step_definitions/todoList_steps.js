module.exports = function () {
  this.Given(/^I am on the todoList page$/, () => {
    return helpers.loadPage('http://react-mobx-todolist.herokuapp.com', 120).then((el) => {
      // inject disable animations
      driver.execute(function() {
        window.Velocity.mock=true;
      });
      return assert.equal(el.state, 'success', el.message);
    })
  });

  this.Given(/^I am on the todoList "(.*)" subpage$/, (page) => {
    return helpers.loadPage('http://react-mobx-todolist.herokuapp.com/#/' + page, 120).then((el) => {
      // inject disable animations
      driver.execute(function() {
        window.Velocity.mock=true;
      });
      return assert.equal(el.state, 'success', el.message);
    })
  });

  this.Given(/^the list contains the task:$/, (table, callback) => {
    let counter = 0;
    let client = driver;
    table.rows().forEach((row) => {
      client = client
        .setValue('input[name=newTodo]', row[0])
        .keys('Enter');
      counter++;
      if (counter === table.rows().length) {
        client.call(callback);
      }
    });
  });

  this.When(/I add "(.*)" to my list$/, (text, callback) => {
    driver
      .waitForExist('input[name=newTodo]')
      .setValue('input[name=newTodo]', text)
      .keys('Enter')
      .call(callback);

  });

  this.When(/^I search for "(.*)"$/, (text, callback) => {
    driver
      .waitForExist('.input.search input')
      .setValue('.input.search input', text)
      .call(callback);
  });

  this.When(/I delete "(.*)" from my list$/, (task, callback) => {
    driver
      .waitForExist('.header=' + task)
      // only xpath allow select sibling item in WebdriverIO for now
      .click('//*[text()[contains(.,"' + task + '")]]/following-sibling::div/i')
      //wait for finish animation
      .waitForVisible('.header=' + task, 600, true)
      .call(callback);
  });

  this.When(/^I set widow to size (\d+) x (\d+) if necessary/, (width, height, callback) => {
    if (driver.getViewportSize('width') !== width || driver.getViewportSize('height') !== width) {
      driver.windowHandleSize({width: width, height: height}).call(callback);
    } else {
      console.log('resolution change is unnecessary');
      driver.call(callback)
    }
  });

  this.When(/^I refresh page$/, (callback) => {
    driver
      .refresh()
      .call(callback);
  });

  this.When(/^I click Clear completed link$/, (callback) => {
    driver
      .waitForExist('a=Clear completed')
      .click('a=Clear completed')
      .call(callback);
  });

  this.When(/I complete "(.*)" from my list$/, (task, callback) => {
    driver
      .waitForExist('.header=' + task)
      .element('.header=' + task)
      .click('i.circle.thin.large.icon')
      .call(callback);
  });

  this.Then(/I see only (\d) task on the list$/, (number, callback) => {
    driver
      .pause(100)
      .elements('.items .item').then((el) => {
      expect(el.value.length).to.equal(Number(number))
    }).call(callback)
  });


  this.Then(/I should see "(.*)" on the list$/, (task, callback) => {
    driver
      .pause(100)
      .waitForExist('.header=' + task)
      .getText('.header=' + task).then((text) => {
      expect(task).to.eql(text)
    })
      .call(callback);

  });

  this.Then(/I see "(.*)" on counter$/, (counterText, callback) => {
    driver
      .pause(100)
      .waitForExist('.task.counter')
      .getText('.task.counter').then((text) => {
        expect(text).to.eql(counterText);
    }).call(callback);
  });


  this.Then(/^I see only this tasks on list:$/, (table, callback) => {
    table.rows().forEach((row) => {
      driver
        .pause(100)
        .waitForExist('.header=' + row[0])
        .getText('.header=' + row[0]).then((text) => {
        expect(row[0]).to.eql(text);
      })
        .call(callback);
    });
  });

  this.Then(/^page still looks nice$/, function (callback) {
    //todo this step need screens comparator mechanism for now it always return success
    driver.call(callback);
  });

};
