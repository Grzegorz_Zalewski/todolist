@Counter
Feature: Task counter
  User should see only active and visible tasks on task counter.

  Scenario: Task counter increment.
    Given I am on the todoList page
    And the list contains the task:
      | task        |
      | first task  |
      | second task |
      | third task  |
    When I add "new test task" to my list
    Then I see "4 items left" on counter


  Scenario: Task counter decrement.
    Given I am on the todoList page
    And the list contains the task:
      | task        |
      | first task  |
      | second task |
      | third task  |
    When I delete "second task" from my list
    Then I see "2 items left" on counter

  Scenario: Task counter decrement on complete task.
    Given I am on the todoList page
    And the list contains the task:
      | task        |
      | first task  |
      | second task |
      | third task  |
    When I complete "second task" from my list
    Then I see "2 items left" on counter