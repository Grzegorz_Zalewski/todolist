@Search
Feature: Search task
  User should have ability to searching tasks by text.

  Scenario: List show only searched task.
    Given I am on the todoList page
    And the list contains the task:
      | task        |
      | black cat   |
      | white dog   |
      | brown cat   |
      | blue cat    |
      | white dog   |
    When I search for "cat"
    Then I see only this tasks on list:
      | task        |
      | black cat   |
      | brown cat   |
      | blue cat    |

  Scenario: List show only searched task active task on active page.
    Given I am on the todoList "active" subpage
    And the list contains the task:
      | task        |
      | black cat   |
      | white dog   |
      | brown cat   |
      | blue cat    |
      | white dog   |
      | white cat   |
    When I complete "black cat" from my list
    And I complete "brown cat" from my list
    And I search for "cat"
    Then I see only this tasks on list:
      | task        |
      | blue cat    |
      | white cat   |