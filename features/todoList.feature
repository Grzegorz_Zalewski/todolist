@BasicUsage
Feature: Basic Usage
  Jako użytkownik aplikacji chciałabym mieć możliwość dodania tekstu zadania do zrobienia
  oraz możliwość usunięcia elementu z listy.

  Scenario: Task can be added.
    Given I am on the todoList page
    When I add "new test task" to my list
    Then I should see "new test task" on the list

  Scenario: Task be removed
    Given I am on the todoList page
    And the list contains the task:
    | task        |
    | first task  |
    | second task |
    | third task  |
    When I delete "first task" from my list
    Then I see only 2 task on the list
    And I see only this tasks on list:
      | task        |
      | second task |
      | third task  |
