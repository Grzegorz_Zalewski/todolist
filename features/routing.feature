@routing
Feature: Page routing
  User visit subpages should see tasks depending on status.

  Scenario: Active page show only active tasks.
    Given I am on the todoList "active" subpage
    And the list contains the task:
      | task        |
      | first task  |
      | second task |
      | third task  |
    When I complete "second task" from my list
    And I complete "first task" from my list
    Then I see only 1 task on the list

  Scenario: Completed page show only completed tasks.
    Given I am on the todoList page
    And the list contains the task:
      | task        |
      | first task  |
      | second task |
      | third task  |
    When I complete "second task" from my list
    And I complete "first task" from my list
    And I am on the todoList "completed" subpage
    Then I see only 2 task on the list