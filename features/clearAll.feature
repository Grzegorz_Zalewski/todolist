@ClearAll
Feature: Clear all
  User should have ability to clear all completed tasks.

  Scenario: List show only active task after click clear completed.
    Given I am on the todoList page
    And the list contains the task:
      | task        |
      | black cat   |
      | white dog   |
      | brown cat   |
      | blue cat    |
      | white dog   |
      | white cat   |
    When I set widow to size 1920 x 1080 if necessary
    And I complete "black cat" from my list
    And I complete "brown cat" from my list
    And I click Clear completed link
    Then I see only this tasks on list:
      | task        |
      | blue cat    |
      | white cat   |